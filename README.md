PRÁCTICA 02
Resolución con monitores

RESOLUCIÓN TEÓRICA

Primero de todo, resolveremos la práctica como hacemos en los ejercicios teóricos. Justificaremos adecuadamente las decisiones. 
Dado que es la práctica 02, estaremos obligados a usar monitores y no otra herramienta para su resolución.

ANÁLISIS DE LA PRÁCTICA A REALIZAR

Se nos presenta un problema de renderizado de escenas, compuestas por fotogramas y con ejecución puramente concurrente. La granja de renderizado,
tiene dos tipos de procesos que gestionan dichas escenas:

- Generadores: Son capaces de crear escenas de dos tipos de prioridad, alta y baja. Dichas escenas serán almacenadas en 2 listas de escenas 
  diferentes de tamaño limitado.
- Renderizadores: Procesarán las escenas en orden de mayor a menor prioridad. Esto sugiere que las listas de escenas serán compartidas 
  por estos dos procesos.

Los elementos destacados son:

- Fotogramas: Tienen un identificador único por cada fotograma y un tiempo de cálculo aleatorio entre MIN_TIEMPO_FOTOGRAMA y 
  MIN_TIEMPO_FOTOGRAMA + VARIACION_TIEMPO segundos.

- Escenas: Tienen un identificador único para cada escena. Un atributo para controlar la prioridad, alta o baja, de la escena,
  que indicará si se atenderán antes o después por los renderizadores. Solo cuando no haya escenas de prioridad alta, se atienden 
  las de prioridad baja. Por definición, una escena es un conjunto de fotogramas con tamaño aletorio entre MIN_NUM_FOTOGRAMAS y 
  MIN_NUM_FOTOGRAMAS + VARIACION_NUM_FOTOGRAMAS. El tiempo que se tarda en renderizar una escena, es la suma del tiempo de cada fotograma, 
  más un tiempo fijo llamado TIEMPO_FINALIZACION_ESCENA.

- Generadores: Crean una escena y la ponen a disposición de los renderizadores. Un generador tarda un tiempo aleatorio entre 
  MIN_TIEMPO_GENERACION Y MIN_TIEMPO_GENERACION + VARIACION_TIEMPO segundos en generar la escena.

- Renderizadores: Toma una escena de las que hay disponibles y la renderizan. Siempre que haya, elige primero de la cola de prioridad alta, 
  si no hay en esa cola, las coge de la de prioridad baja. Una escena no está completa hasta que no se han renderizado todos sus fotogramas. 
  Cuando una escena ha sido renderizada, se coloca en una lista de escenas resultado.

Los generadores de escenas están conectados con los renderizadores de escenas mediante dos listas de escenas con acceso compartido. Hay dos 
listas según la prioridad. Los generadores escriben escenas en las listas y los renderizadores las leen. ( Problema clásico de “Productor y 
consumidor” ). Esas listas tienen un tamaño máximo de MAX_ESCENAS_EN_ESPERA. Si ya hay MAX_ESCENAS_EN_ESPERA en una lista y un generador tiene
que escribir en ella, deberá esperar a que salga aluna escena antes de poder seguir escribiendo en la lista.

PROCESO GENERAL

Se construirán NUM_GENERADORES Generadores de escenas. Se construirán NUM_RENDERIZADORES Renderizadores de escenas. Cada Generador de escenas
construirá un número de escenas comprendido entre MIN_NUM_ESCENAS y MIN_NUM_ESCENAS + VARIACION_NUM_ESCENAS. 
Cuando los generadores han generado todas sus escenas, terminan su ejecución. Cuando no quedan escenas en ninguna de las listas y los generadores
han terminado su ejecución, los renderizadores terminan su ejecución.

Al finalizar la ejecución de todas las tareas, el programa mostrará: 

•	El número total de escenas procesadas. 
•	El tiempo total empleado en Renderizar todas las escenas. 

Para cada escena:

 - La hora en que se generó. 
 - La hora en que un Renderizador comenzó su procesamiento. 
 - Los fotogramas que la componen y la duración de cada uno.
 - La hora en la que el Renderizador terminó su procesamiento. 
 - El tiempo total empleado en el procesamiento de la escena.


CLASES , MÉTODOS Y ESTRUCTURAS DE DATOS

ESTRUCTURAS DE DATOS

-  Buffer <TipoDato> : Estructura que va a permitir almacenar elementos de TipoDato, probablemente implementada como una lista o un array. Esta estructura recuerda a la utilizada en los problemas tipo productor y consumidor, ya que mantiene un orden en la inserción y la extracción.
-  BufferAltaPrioridad <Escena> : Al igual que el buffer anterior, utilizaremos esta estructura para almacenar elementos siguiendo un orden, en este caso las escenas de prioridad alta. Los métodos y funciones serán los mismos de la estructura.
-  BufferBajaPrioridad <Escena> : Lo mismo que el buffer anterior pero para escenas de baja prioridad.

MÉTODOS O FUNCIONES

o	ADD(TipoDato): inserción ordenada en el Buffer del elemento TipoDato.
o	GET(): TipoDato. Devuelve en orden de extracción el elemento de la estructura de datos y lo elimina de ésta.
o	EMPTY(): Boolean. Nos dice si el buffer está vacío o no.

CLASES

- Fotograma : Clase para controlar los fotogramas generados.

•	ATRIBUTOS

o	IDFotograma: int.
o	Tiempo: int.

•	MÉTODOS O FUNCIONES

o	GetTiempoCalculo(): Devuelve el tiempo de cálculo del fotograma.

-  Escena : Clase para controlar los fotogramas generados.

•	ATRIBUTOS

o	IDEscena: int.
o	TiempoTotal: int.
o	Prioridad: Boolean. True = alta , False = baja.
o	BufferFotogramas: Buffer<Fotograma>.

•	MÉTODOS O FUNCIONES

o	GeneraFotogramas(): Crea los fotogramas de la escena adecuada.
o	FinalizaFotograma(): Tiempo finalización fotogramas.

-  Generador : Clase para generar escenas.

•	ATRIBUTOS

o	Tiempo: int. Tiempo recopilación fotogramas.

•	MÉTODOS O FUNCIONES

o	GeneraEscenas(): Método principal. Genera todas las escenas.

-  Procesador : Clase para procesar las escenas.

•	ATRIBUTOS

o	TiempoTotal: int. Tiempo total de procesar una escena.

•	MÉTODOS O FUNCIONES

o	ProcesaEscena(): Lee y procesa las escenas.

-  Buffers : Clase para controlar los buffer y las estructuras que los trabajan.

•	ATRIBUTOS

o	BufferAlta: Buffer<Escena>. Buffer de escenas de alta prioridad.
o	BufferBaja: Buffer<Escena>. Buffer de escenas de baja prioridad.

-  Principal : Clase principal que lanzará todos los recursos y controlará la ejecución y finalización del resto de procesos y clases.

•	ATRIBUTOS

o	Buffer: Buffers.

•	MÉTODOS O FUNCIONES

o	EjecutaGeneradores(). Crea los generadores de escenas y los lanza.
o	EjecutaRenderizadores(). Crea los renderizadores y los lanza.
o	FinalizaGeneradores(). Controla y finaliza los generadores lanzados anteriormente.
o	FinalizaRenderizadores(). Controla y finaliza los renderizadores lanzados anteriormente.

-  Finalizador : Clase que se encargará de recopilar al información requerida en este ejercicio de la práctica para poder presentársela al usuario.

•	ATRIBUTOS

o	TiempoTotal: int.
o	FechaGeneración: Date.
o	FechaInicioProceso: Date.
o	FechaFinProceso: Date.
o	BufferResultado: Buffer <Escena>.

•	MÉTODOS O FUNCIONES

o	TiempoTotalEscena(): int. Tiempo total de la escena. 

VARIABLES COMPARTIDAS

Cuando tratamos con concurrencia como en este caso, muchas veces existen recursos como la memoria, que son compartidos por varios procesos, por lo
que un proceso no podrá estar escribiendo en una zona de memoria si un proceso está leyendo esa misma zona de memoria en el mismo instante. 
En el caso de nuestra práctica, existen recursos compartidos entre las clases Generador, Procesador y la Principal, dichos recursos son:

- BufferAlta: Buffer<Escena>. Almacena las escenas de prioridad alta donde escribirán los generadores ( para meter las escenas generadas) y leerán los renderizadores ( para sacar las escenas y renderizarlas ).
- BufferBaja: Buffer<Escena>. Almacena las escenas de prioridad baja donde escribirán los generadores ( para meter las escenas generadas) y leerán los renderizadores ( para sacar las escenas y renderizarlas ).

MONITORES

Para evitar el problema que se nos plantea en el enunciado anterior, plantearemos monitres, que permiten compartir recursos entre procesos sin problema
ninguno. Aunque no es el único problema que se nos plantea en esta práctica, por lo que habrá que implementar algunos monitores más.

	Barrier: Barrera para indicar a los renderizadores que los generadores ya lo han generado todo

	PrioridadAltaVacía: Este monitor regulará el acceso de los generadores al buffer de escenas de alta prioridad. Si la lista está vacía o hay 
    algún hueco, podrán acceder, si está llena, no. El monitor representará el número de posiciones libres en dicho buffer por lo que tendrá que
    inicializarse a MAX_ESCENAS_EN_ESPERA.

	PrioridadAltaLlena: Monitor totalmente contrario al anterior, en este caso regulará el acceso de los procesadores, 
    que si la lista está vacía, no podrán acceder, pero si está llena o hay algún elemento, sí. Como este monitor representa el número de elementos
    en el buffer, al contrario que el anterior, se inicializará en cero.

	PrioridadBajaVacía: Lo mismo que PrioridadAltaVacía pero para la prioridad baja.

	PrioridadBajaLlena: Lo mismo que PrioridadAltaLlena pero para la prioridad baja.

Estos son los monitores que he comentado antes para el acceso a los recursos compartidos, serán monitores de exclusión mutua por lo tanto:

	EXCMBufferAlta: Necesario para garantizar el acceso seguro al buffer de prioridad alta. Se inicializa en uno.

	EXCMBufferBaja: Necesario para garantizar el acceso seguro al buffer de prioridad baja. Se inicializa en uno.


ESPECIFICACIONES DE PROCEDIMIENTOS

En el análisis anterior de las clases y sus métodos, hemos sido escuetos en cuanto al funcionamiento de los distintos procesos. Como el último
paso para concluir nuestro informe de análisis y diseño es el de implementar el pseudocódigo de la práctica, es conveniente que previamente
entremos un poco más en profundidad en el funcionamiento de estos métodos y procesos, hablando de ellos como procesos. Además también mencionaremos
aspectos no tratados anteriormente.

En el proceso Principal (hilo principal):

	CrearProceso( Generadores, Procesadores ): Creará los procesos Generador y Procesador.
	EjecutarProceso( Generadores, Procesadores ): Ejecutará los procesos correspondientes y los lanzará.
	EsperarProceso( Escenas, Procesadores): Esperará a que todos los fotogramas de todas las escenas correspondientes hayan sido finalmente procesados.
	FinalizarProceso( Generadores, Procesadores): Finalizará todos los procesos del sistema.

En el proceso Escena:

	GeneraFotogramas(): crea un número aleatorio de fotogramas entre MIN_NUM_FOTOGRAMAS y MIN_NUM_FOTOGRAMAS + VARIACION_NUM_FOTOGRAMAS que representa los fotogramas que forman la Escena final.
	FinalizaFotogramas(): permite indicar el tiempo de finalización de fotogramas solicitados.

En el proceso Generador:

	GeneraEscenas(): permite crear un número de escenas entre MIN_NUM_ESCENAS y MIN_NUM_ESCENAS + VARIACION_NUM_ESCENAS.
	GeneraEscena(): genera una escena como tal y la agrega al buffer que corresponda.

En el proceso Procesador:

	ProcesaEscena(): Procesa una escena de la buffer correspondiente según prioridades.

En el proceso Finalizador:

	MostrarDatos( Resultados ): Este procedimiento presenta de forma legible los datos solicitados.

En el proceso Monitor:

 CargarEscenaPrioridadAlta(Escena e): Accede al recurso BufferAlta para cargar una escena de prioridad alta.
 CargarEscenaPrioridadAlta(Escena e): Accede al recurso BufferBaja para cargar una escena de prioridad baja.
 PrioridadAltaVacía(): Verifica si hay elementos en BufferAlta.
 PrioridadBajaVacía(): Verifica si hay elementos en BufferBaja.
 ExtraerEscenaAltaPrioridad(): Saca una escena del BufferAlta.
 ExtraerEscenaBajaPrioridad(): Saca una escena del BufferBaja.
 SolicitarAccesoListas(): Bloquea el acceso compartido si las listas de escenas pendientes están vacías.
 LiberarAccesoListas(): Libera el acceso de la comprobación anterior

DISEÑO Y PSEUDOCÓDIGO

Resolución en pseudocódigo de la práctica dónde se intentará abordar la ejecución y finalización correcta de todos los procesos.

Sistema (Hilo principal)

 - Ejecucion():

    CrearProceso( Monitor , Generadores, Procesadores )
    EjecutarProceso( Monitor , Generadores , Procesadores )
    
    while( WaitProceso(Monitor,Generadores,Procesadores) )
    {
        #######
    }
    
    FinalizarProceso(Monitor , Generadores , Procesadores )
    MostrarDatos();
    
Proceso Escena ( id , prioridad )

 - Variables locales:

    int ID;
    boolean prioridad;
    int TiempoTotal;
    BufferFotogramas Bufferfotogramas;
    Semaforo EXCMBufferFotogramas; //Acceso por exclusión mutua
    
- Ejecucion():

    for( i = 1 , n_fotogramas )
    {
        Fotograma f = new Fotograma(i);
        TiempoTotal = TiempoTotal + tiempo_generación_fotograma;
        Bufferfotogramas.add(f);
    }
    
Proceso Generador ( Monitor )

 - Variables locales:

    int Tiempo;
    BufferEscenas resultado;
    
- Ejecucion():

    for( i = 1 , n_escenas )
    {
        Escena e;
        resultado.add(e);
        
        if(e.prioridad() == true)
        {
            monitor.CargarEscenaAltaPrioridad(escena)
        }
        else
        {
            monitor.CargarEscenaBajaPrioridad(escena)
        }
    }
    
    barrier.wait();
    return resultado; 
    
Proceso Renderizador ( Monitor )

 - Variables locales:

    int Tiempo;
    Buffer resultado;
    Boolean HayEscena;
    
    int Horainicio;
    int Horafin;
    
- Ejecucion():

    while( true )
    {
    
        Monitor.solicitaAccesoListas();
        Escena e;
        
        Horainicio = now();
        
        if(Monitor.HayEscenaListaAltaPrioridad)
        {
            e = Monitor.SacarEscenaAltaPrioridad();
            HayEscena = true;
        }
        else
        {
            if(Monitor.HayEscenaListaBajaPrioridad)
            {
                e = Monitor.SacarEscenaBajaPrioridad();
                HayEscena = true;
            }
            else
            {
                if(GeneradorEnd)
                {
                    Monitor.LiberaListas();
                    return resultado;
                }
                
            }
        }
    
        Monitor.LiberaListas();
        
        if(HayEscena)
        {
            Sleep( tiempo renderizado );
            Sleep( tiempo escena);
            Horafin = now();
            resultado.add(e);
        }
        else
        {
            EsperaActiva();
        }
    
    false;
    
    }
    
Proceso Fotograma ( ID )

 - Variables locales:

    int ID;
    int Tiempo;

- Ejecucion():

    Tiempo(){return Tiempo;}
    ID(){return ID;}
    
Proceso Monitor ( Escenas wait )

 - Variables locales:

    BufferEscenas BufferAltaPrioridad;
    BufferEscenas BufferBajaPrioridad;
    
    Condition LLenoAltaPrioridad;
    Condition LLenoBajaPrioridad;
    Condition VacioAltaPrioridad;
    Condition VacioBajaPrioridad;
    Condition MutexAltaPrioridad;
    Condition MutexBajaPrioridad;
    Condition Principal;
    
- Ejecucion():

    EscenaCargaAltaPrioridad( Escena e )
    {
        delay LLenoAltaPrioridad;
        delay MutexAltaPrioridad;
        
        BufferAltaPrioridad.add(e);
        
        resume MutexAltaPrioridad;
        resume LLenoAltaPrioridad;
    }
    
    EscenaCargaBajaPrioridad( Escena e )
    {
        delay LLenoBajaPrioridad;
        delay MutexBajaPrioridad;
        
        BufferBajaPrioridad.add(e);
        
        resume MutexBajaPrioridad;
        resume LLenoBajaPrioridad;
    }
    
    AltaPrioridadVacia()
    {
        return BufferAltaPrioridad.isEmpty();
    }
    
    BajaPrioridadVacia()
    {
        return BufferBajaPrioridad.isEmpty();
    }
    
    Escena ExtraerEscenaAltaPrioridad()
    {
        if(Interrupción)
        {Lanzar excepcion}
        
        Escena e;
        
        delay AltaPrioridadVacia;
        delay MutexAltaPrioridad.acquire;
        
        e = BufferAltaPrioridad.get(primerelemento);
        BufferAltaPrioridad.remove(primerelemento);
        
        resume MutexAltaPrioridad;
        resume AltaPrioridadLlena;
        
        return e;
    }
    
    Escena ExtraerEscenaBajaPrioridad()
    {
        if(Interrupción)
        {Lanzar excepcion}
        
        Escena e;
        
        delay BajaPrioridadVacia;
        delay MutexBajaPrioridad.acquire;
        
        e = BufferBajaPrioridad.get(primerelemento);
        BufferBajaPrioridad.remove(primerelemento);
        
        resume MutexBajaPrioridad;
        resume BajaPrioridadLlena;
        
        return e;
    }
    
    OcupaAccesoLista()
    {delay Principal.acquire;}
    
    LiberAccesoLista()
    {delay Principal.release;}
    
    