package ESG;

import static ESG.Constantes.MAX_ESCENAS_EN_ESPERA;
import static ESG.Constantes.NUM_GENERADORES;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


public class Main {
      
    static void PreparaGeneradores( LinkedList<Renderizador> renderizadores , Monitor m)
    {
        for (int i = 0; i < NUM_GENERADORES; i++)
        {
            Renderizador r = new Renderizador(m);
            renderizadores.add(r);
        }
    }
    
    static void CargaGeneradores( Future<List<Escena>> iniciales , Monitor m , CyclicBarrier barrier , Finalizador termina ,  ExecutorService principal)
    {
        for (int i = 0; i < NUM_GENERADORES; i++) 
        {
            iniciales = principal.submit(new Generador( barrier , termina , m));
        }
    }
    
    public static void main(String[] args) throws InterruptedException, ExecutionException, BrokenBarrierException, TimeoutException {
    
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");
        
        //PREPARAMOS LAS ESTRUCTURAS Y LOS DATOS A TRABAJAR
        LinkedList<Renderizador> renderizadores = new LinkedList();
        
        //PUESTA A PUNTO, TODOS LOS PROCESOS GENERALES
        ExecutorService principal = (ExecutorService) Executors.newCachedThreadPool();
        CyclicBarrier barrier= new CyclicBarrier(NUM_GENERADORES);
        Monitor m = new Monitor(MAX_ESCENAS_EN_ESPERA);
        
        //COMENZAMOS CON EL MEJUNGE
        PreparaGeneradores( renderizadores , m);
        
        Future<List<Escena>> iniciales = null; 
        Finalizador termina= new Finalizador(renderizadores); 
        
        CargaGeneradores( iniciales ,  m ,  barrier ,  termina ,   principal);
        
        //PREPARAMOS EL TRATAMIENTO DE DATOS AL FINAL DE LA EJECUCIÓN  
        
        List<Future<List<Escena>>> finales = principal.invokeAll(renderizadores);
        
        //APAGAMOS LAS MAQUINAS
        principal.shutdown();      
        principal.awaitTermination(1, TimeUnit.SECONDS);
        
        //A PARTIR DE AQUI; SOLO TRATAMOS LOS DATOS TRABAJADOS
        
        float result = 0f;
        int n_escenas=0;
        for ( Future<List<Escena>> escena : finales) 
        {
             List<Escena> lista=escena.get();
             n_escenas+=lista.size();
             for(int i = 0; i<lista.size(); i++)
             {
                result+=lista.get(i).TotalProcesamiento();
                System.out.println("La escena " + lista.get(i).getCadena() + " generó: " + lista.get(i).toString());
             }
        }
        System.out.println("Trabajo(procesamiento total): "+ result );
        System.out.println("Nº escenas: "+ n_escenas);
        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
    }      
    
}
