package ESG;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Semaphore;

public class Monitor {
    
    //LISTAS COMPARTIDAS
    static List<Escena> listaAltaPrioridad;
    static List<Escena> listaBajaPrioridad;
    
    //SEMÁFOROS ( DUDA DE SI VAN AQUÍ O EN EL MAIN O SE PASAN POR CABECERA O QUE)
    static Semaphore LlenarAltaPrioridad;
    static Semaphore EXCMAltaPrioridad;
    static Semaphore VaciarAltaPrioridad;
    static Semaphore LlenarBajaPrioridad;
    static Semaphore EXCMBajaPrioridad;
    static Semaphore VaciarBajaPrioridad;
    static Semaphore Principal;
        
    public Monitor(int escenas){
        
        LlenarAltaPrioridad = new Semaphore(escenas);
        EXCMAltaPrioridad = new Semaphore(1);
        VaciarAltaPrioridad = new Semaphore(0);
        
        LlenarBajaPrioridad = new Semaphore(escenas);
        EXCMBajaPrioridad = new Semaphore(1);
        VaciarBajaPrioridad = new Semaphore(0);
        
        Principal = new Semaphore(1);
        
        listaAltaPrioridad= new LinkedList<>();
        listaBajaPrioridad= new LinkedList<>();

    }   
    
    public void AdquirirAccesoListas() throws InterruptedException
    {Principal.acquire();}
    
    public void AbrirAccesoListas() throws InterruptedException
    {Principal.release();}
    
    public boolean listaAltaPrioridadLlena()
    {return !(listaAltaPrioridad.isEmpty());}
    
    public boolean listaBajaPrioridadLlena()
    {return !(listaBajaPrioridad.isEmpty());}
    
    public Escena SacarAltaPrioridad() throws InterruptedException
    {
        Escena e = null;
        
        //Controlamos la interrupción como en prácticas
        if(Thread.interrupted())
        {throw new InterruptedException();}
        
        VaciarAltaPrioridad.acquire();
        EXCMAltaPrioridad.acquire();
        try
        {
            e = listaAltaPrioridad.get(0);
            listaAltaPrioridad.remove(0);
        }finally
        {
            EXCMAltaPrioridad.release();
            LlenarAltaPrioridad.release();
        }
        
        return e;
    }
    
    public Escena SacarBajaPrioridad() throws InterruptedException
    {
        Escena e = null;
        
        //Controlamos la interrupción como en prácticas
        if(Thread.interrupted())
        {throw new InterruptedException();}
        
        VaciarBajaPrioridad.acquire();
        EXCMBajaPrioridad.acquire();
        try
        {
            e = listaBajaPrioridad.get(0);
            listaBajaPrioridad.remove(0);
        }finally
        {
            EXCMBajaPrioridad.release();
            LlenarBajaPrioridad.release();
        }
        
        return e;
    }
    
    public void CargaEscenaPrioridadAlta( Escena e ) throws InterruptedException
    {
        LlenarAltaPrioridad.acquire();
        EXCMAltaPrioridad.acquire();
        try{
            System.out.println("Insertando " + e.getCadena() + "en la lista de alta prioridad \n");
            listaAltaPrioridad.add(e);
        }finally
        {
            EXCMAltaPrioridad.release();
            VaciarAltaPrioridad.release();
        }

        System.out.println("Escena " + e.getCadena() + " insertada ");
    }
    
    public void CargaEscenaPrioridadBaja( Escena e ) throws InterruptedException
    {
        LlenarBajaPrioridad.acquire();
        EXCMBajaPrioridad.acquire();
        try{
            System.out.println("Insertando " + e.getCadena() + "en la lista de alta prioridad \n");
            listaBajaPrioridad.add(e);
        }finally
        {
            EXCMBajaPrioridad.release();
            VaciarBajaPrioridad.release();
        }

        System.out.println("Escena " + e.getCadena() + " insertada ");
    }
    
}