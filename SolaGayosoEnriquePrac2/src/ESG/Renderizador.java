package ESG;

import static ESG.Escena.ident;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;


public class Renderizador implements Callable<List<Escena>>{
    public static final int RETRASO = 1;
    
    //CADENA IDENTIFICADOR
    private final String cadena;
    //FINALIZADOR
    private boolean terminadosGeneradores;
    List<Escena> resultado;

    Monitor monitor;
    
    
  public Renderizador(Monitor monitor){
    cadena=""+ident++;  
    terminadosGeneradores= false; 
    
    this.resultado=new LinkedList<>();

    this.monitor= monitor;
  }

  public List<Escena> call() throws InterruptedException {
    Escena escena= null;
    boolean tengoEscena= false;
    while (true){
        tengoEscena= false;
        monitor.AdquirirAccesoListas(); 
        if (monitor.listaAltaPrioridadLlena()){ 
            escena= monitor.SacarAltaPrioridad();
            tengoEscena= true;
        }else
        {
            if(monitor.listaBajaPrioridadLlena())
            {
                escena=monitor.SacarBajaPrioridad();
                tengoEscena= true;
            }else
            {
                if(terminadosGeneradores)
                {
                    monitor.AbrirAccesoListas();
                    return resultado;
                }
            }
        }
        monitor.AbrirAccesoListas();
        
        if (tengoEscena)
        {
            System.out.println("Procesando Escena "+escena.getCadena());
            escena.setInicio(new Date());
            TimeUnit.SECONDS.sleep(escena.getDuracion());
            escena.setFin(new Date());
            resultado.add(escena);
        }else
        {
            TimeUnit.SECONDS.sleep(RETRASO);
        }        
    }  //fin while
      
  }
  
  public void setTerminadosGeneradores()
  {
      terminadosGeneradores= true;
  }

}